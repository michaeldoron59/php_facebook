<?php
    include_once 'runScript.php';
    @session_start();     
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        
        <title>Registration</title>
        <link type="text/css" rel="stylesheet" href="register.css">
        <script type="text/javascript" src="js/libs/jquery/jquery.js"></script>
        <script type="text/javascript" src="js/libs/myFunction.js"></script>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <script>
            
            $(document).ready(function (){
                $('#fileImage').hide();
                $('#slecteImage').click(function (){
                    $('#fileImage').click();
                    return false;
                });
                
                
                $('#regForm').submit(function (e){
                    if($.trim($('#Username').val())==="")
                    {
                        addError('#Username','#UsernameError',"Invalid user name. must be non-empty.");
                   
                    }
                    return ($('.errorHighlight').length===0);
                });
               
            });        
            
        </script>
    <?php
        if(isset($_SESSION['Massage'])&&($_SESSION['Massage']=="registerError")){
    ?>
       <script>
         $(document).ready(function (){ 
            $('#registerError').css('visibility','visible');
            $('#registerError').html("Registration failed. Please try again");
        });
       </script>
    <?php
        $_SESSION['Massage']="";
        }
    ?>
    </head>
    <body>

 
        <div class="ui-widget">
            <div class="logo">
                <div class="form-sub-title">
                    Facebook
                </div>
            </div>
            <div id="div-regForm">
                
                <div class="form-title">Sign Up</div>
                <div class="form-sub-title">It's free and anyone can join</div>

                <form id="regForm" action="runScript.php" method="post">
                    
                    <div class="input-container">
                        <span id="registerError" class="error"></span>
                    </div>
                    <div class="input-container">
                        <input type="text" id="Username" name="Username" placeholder="user name" onfocus="removeError('#Username','#UsernameError')"/>
                        <span id="UsernameError" class="error"></span>
                    </div>
                    <div class="input-container">
                        <input type="text" id="Email" name="Email" placeholder="example@abc.com" onblur="validateEmail()" onfocus="removeError('#Email','#EmailError')" />
                        <span id="EmailError" class="error"></span>
                    </div>
                    <div class="input-container">
                        <input type="password" id="password" name="Password" placeholder="password" onblur="validatePasswor()" onfocus="removeError('#password','#PasswordError')"/>
                        <span id="PasswordError" class="error"></span>
                    </div> 
                    <div class="input-container">
                        <button id="slecteImage" class="lightBlueButton"> Choose profile image</button>
                        <input id ="fileImage" type="file"  name="FileToUpload" >
                    </div>
                    
                    <div class="input-container">
                        <input type="submit" value="submit" class="greenButton">
                    </div>   
                    <input type="hidden" name="doPhpCode" value="register();">
                </form>

            </div>
        </div>
    </body>
    
</html>
