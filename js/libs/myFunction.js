/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
function addError(inputID,errorID,massegeError){
    $(inputID).addClass("errorHighlight");
    $(errorID).css('visibility','visible');
    $(errorID).html(massegeError);
}
function removeError(inputID,errorID){
    if($(inputID).hasClass('errorHighlight'))
    {
        $(inputID).removeClass("errorHighlight");
        $(errorID).css('visibility','hidden');
        if(inputID==='#password')
        {
            $(inputID).val("");
        }
    }
}
function validatePasswor()
{
    var pass=$.trim($('#password').val());
    if(pass===""){
         addError('#password','#PasswordError',"Invalid password. must be non-empty.");
         return;
    }   
    var filter=new RegExp(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]{8,})$/);
    if(!filter.test(pass)){  
        addError('#password','#PasswordError',"Invalid password. Length should be at least 8 characters,<br> and contain at least one letter and one digit.");
    }
}
function validateEmail()
{
    var email=$.trim($('#Email').val());
    massege="";
    if(email===""){
        massege="Invalid email. must be non-empty.";    
    }
    var filter = new RegExp(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/);
    if(!filter.test(email)&&massege==="")
    { 
        massege="Invalid email address. Should be characters@text.text";
    }
    if(!(massege===""))
    {
        addError('#Email','#EmailError',massege);
    }
    $.post('runScript.php', {doPhpCode:"emailExist();", emailExist:email }, function(isExist) 
    {
        if(isExist==='true')
        {
            addError('#Email','#EmailError','The email is already registered');
        }    
    });  
}
